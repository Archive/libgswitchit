/*
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifndef __GNOME_KBD_INDICATOR_H__
#define __GNOME_KBD_INDICATOR_H__

#include <gtk/gtk.h>

#include <libxklavier/xklavier.h>

#ifdef __cplusplus
extern "C" {
#endif

	typedef struct _GnomeKbdIndicator GnomeKbdIndicator;
	typedef struct _GnomeKbdIndicatorPrivate GnomeKbdIndicatorPrivate;
	typedef struct _GnomeKbdIndicatorClass GnomeKbdIndicatorClass;

#define GNOME_TYPE_KBD_INDICATOR             (gnome_kbd_indicator_get_type ())
#define GNOME_KBD_INDICATOR(obj)             (G_TYPE_CHECK_INSTANCE_CAST ((obj), GNOME_TYPE_KBD_INDICATOR, GnomeKbdIndicator))
#define GNOME_KBD_INDICATOR_CLASS(obj)       (G_TYPE_CHECK_CLASS_CAST ((obj), GNOME_TYPE_KBD_INDICATOR,  GnomeKbdIndicatorClass))
#define GNOME_IS_KBD_INDICATOR(obj)          (G_TYPE_CHECK_INSTANCE_TYPE ((obj), GNOME_TYPE_KBD_INDICATOR))
#define GNOME_IS_KBD_INDICATOR_CLASS(obj)    (G_TYPE_CHECK_CLASS_TYPE ((obj), GNOME_TYPE_KBD_INDICATOR))
#define GNOME_KBD_INDICATOR_GET_CLASS(obj)   (G_TYPE_INSTANCE_GET_CLASS ((obj), GNOME_TYPE_KBD_INDICATOR, GnomeKbdIndicatorClass))

	struct _GnomeKbdIndicator {
		GtkNotebook parent;
		GnomeKbdIndicatorPrivate *priv;
	};

	struct _GnomeKbdIndicatorClass {
		GtkNotebookClass parent_class;

		void (*reinit_ui) (GnomeKbdIndicator * gki);
	};

	extern GType gnome_kbd_indicator_get_type (void);

	extern GtkWidget *gnome_kbd_indicator_new (void);

	extern void gnome_kbd_indicator_reinit_ui (GnomeKbdIndicator *
						   gki);

	extern void gnome_kbd_indicator_set_angle (GnomeKbdIndicator * gki,
						   gdouble angle);

	extern XklEngine *gnome_kbd_indicator_get_xkl_engine (void);

	extern gchar **gnome_kbd_indicator_get_group_names (void);

	extern gchar *gnome_kbd_indicator_get_image_filename (guint group);

	extern gdouble
	    gnome_kbd_indicator_get_max_width_height_ratio (void);

	extern void
	 gnome_kbd_indicator_set_parent_tooltips (GnomeKbdIndicator *
						  gki, gboolean ifset);

	extern void
	 gnome_kbd_indicator_set_tooltips_format (const gchar str[]);

#ifdef __cplusplus
}
#endif
#endif
