#ifndef __KEYBOARD_CONFIG_REGISTRY_H__
#define __KEYBOARD_CONFIG_REGISTRY_H__

#include <dbus/dbus-glib-bindings.h>
#include <libxklavier/xklavier.h>

typedef struct KeyboardConfigRegistry KeyboardConfigRegistry;
typedef struct KeyboardConfigRegistryClass KeyboardConfigRegistryClass;

struct KeyboardConfigRegistry {
	GObject parent;

	XklEngine *engine;
	XklConfigRegistry *registry;
};

struct KeyboardConfigRegistryClass {
	GObjectClass parent;
	DBusGConnection *connection;
};

#define KEYBOARD_CONFIG_TYPE_REGISTRY              (keyboard_config_registry_get_type ())
#define KEYBOARD_CONFIG_REGISTRY(object)           (G_TYPE_CHECK_INSTANCE_CAST ((object), KEYBOARD_CONFIG_TYPE_REGISTRY, KeyboardConfigRegistry))
#define KEYBOARD_CONFIG_REGISTRY_CLASS(klass)      (G_TYPE_CHECK_CLASS_CAST ((klass), KEYBOARD_CONFIG_TYPE_REGISTRY, KeyboardConfigRegistryClass))
#define KEYBOARD_CONFIG_IS_REGISTRY(object)        (G_TYPE_CHECK_INSTANCE_TYPE ((object), KEYBOARD_CONFIG_TYPE_REGISTRY))
#define KEYBOARD_CONFIG_IS_REGISTRY_CLASS(klass)   (G_TYPE_CHECK_CLASS_TYPE ((klass), KEYBOARD_CONFIG_TYPE_REGISTRY))
#define KEYBOARD_CONFIG_REGISTRY_GET_CLASS(obj)    (G_TYPE_INSTANCE_GET_CLASS ((obj), KEYBOARD_CONFIG_TYPE_REGISTRY, KeyboardConfigRegistryClass))


/**
 * DBUS server
 */

extern GType keyboard_config_registry_get_type (void);

extern gboolean
    keyboard_config_registry_get_current_descriptions_as_utf8
    (KeyboardConfigRegistry * registry,
     gchar *** short_layout_descriptions,
     gchar *** long_layout_descriptions,
     gchar *** short_variant_descriptions,
     gchar *** long_variant_descriptions, GError ** error);

#endif
