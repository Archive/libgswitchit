/*
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifndef __GSWITCHIT_UTIL_H__
#define __GSWITCHIT_UTIL_H__

#include <glib.h>
#include <gtk/gtkwidget.h>

extern void gswitchit_help (GtkWidget * app_widget, const gchar * bookmark);

extern void gswitchit_install_glib_log_appender (void);

extern GdkRectangle *gswitchit_preview_load_position (void);

extern void gswitchit_preview_save_position (GdkRectangle * rect);


#endif
