/*
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifndef __GSWITCHIT_PLUGIN_MANAGER_H__
#define __GSWITCHIT_PLUGIN_MANAGER_H__

#include <gmodule.h>
#include <libgswitchit/gswitchit-plugin.h>

typedef struct _GSwitchItPluginManager {
	GHashTable *all_plugin_recs;
	GSList *inited_plugin_recs;
} GSwitchItPluginManager;

typedef struct _GSwitchItPluginManagerRecord {
	const char *full_path;
	GModule *module;
	const GSwitchItPlugin *plugin;
} GSwitchItPluginManagerRecord;

extern void
 gswitchit_plugin_manager_init (GSwitchItPluginManager * manager);

extern void
 gswitchit_plugin_manager_term (GSwitchItPluginManager * manager);

extern void
 gswitchit_plugin_manager_init_enabled_plugins (GSwitchItPluginManager * manager,
						GSwitchItPluginContainer *
						pc,
						GSList * enabled_plugins);

extern void
 gswitchit_plugin_manager_term_initialized_plugins (GSwitchItPluginManager * manager);

extern void
 gswitchit_plugin_manager_toggle_plugins (GSwitchItPluginManager * manager,
					  GSwitchItPluginContainer * pc,
					  GSList * enabled_plugins);

extern const GSwitchItPlugin
    *gswitchit_plugin_manager_get_plugin (GSwitchItPluginManager * manager,
					  const char *full_path);

extern void
 gswitchit_plugin_manager_promote_plugin (GSwitchItPluginManager * manager,
					  GSList * enabled_plugins,
					  const char *full_path);

extern void
 gswitchit_plugin_manager_demote_plugin (GSwitchItPluginManager * manager,
					 GSList * enabled_plugins,
					 const char *full_path);

extern void
 gswitchit_plugin_manager_enable_plugin (GSwitchItPluginManager * manager,
					 GSList ** enabled_plugins,
					 const char *full_path);

extern void
 gswitchit_plugin_manager_disable_plugin (GSwitchItPluginManager * manager,
					  GSList ** enabled_plugins,
					  const char *full_path);

extern void
 gswitchit_plugin_manager_configure_plugin (GSwitchItPluginManager * manager,
					    GSwitchItPluginContainer * pc,
					    const char *full_path,
					    GtkWindow * parent);

// actual calling plugin notification methods

extern void
 gswitchit_plugin_manager_group_changed (GSwitchItPluginManager * manager,
					 GtkWidget * notebook,
					 int new_group);

extern void
 gswitchit_plugin_manager_config_changed (GSwitchItPluginManager * manager,
					  GSwitchItKbdConfig * from,
					  GSwitchItKbdConfig * to);

extern int
 gswitchit_plugin_manager_window_created (GSwitchItPluginManager * manager,
					  Window win, Window parent);

extern GtkWidget
    *gswitchit_plugin_manager_decorate_widget (GSwitchItPluginManager *
					       manager, GtkWidget * widget,
					       const gint group,
					       const char
					       *group_description,
					       GSwitchItKbdConfig *
					       config);

#endif
