/*
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifndef __GSWITCHIT_CONFIG_PRIVATE_H__
#define __GSWITCHIT_CONFIG_PRIVATE_H__

#include "libgswitchit/gswitchit-config.h"

extern const gchar GSWITCHIT_PREVIEW_CONFIG_DIR[];
extern const gchar GSWITCHIT_PREVIEW_CONFIG_KEY_X[];
extern const gchar GSWITCHIT_PREVIEW_CONFIG_KEY_Y[];
extern const gchar GSWITCHIT_PREVIEW_CONFIG_KEY_WIDTH[];
extern const gchar GSWITCHIT_PREVIEW_CONFIG_KEY_HEIGHT[];

/*
 * Applet configuration
 */
typedef struct _GSwitchItAppletConfig {
	int secondary_groups_mask;
	gboolean show_flags;

	GSList *enabled_plugins;

	/* private, transient */
	GConfClient *conf_client;
	GSList *images;
	GtkIconTheme *icon_theme;
	int config_listener_id;
	XklEngine *engine;
} GSwitchItAppletConfig;

/**
 * General config functions (private)
 */
extern void
 gswitchit_config_add_listener (GConfClient * conf_client,
				const gchar * key,
				GConfClientNotifyFunc func,
				gpointer user_data, int *pid);


extern void
 gswitchit_config_remove_listener (GConfClient * conf_client, int *pid);

extern void gswitchit_kbd_config_model_set (GSwitchItKbdConfig *
					    kbd_config,
					    const gchar * model_name);

extern void gswitchit_kbd_config_layouts_reset (GSwitchItKbdConfig *
						kbd_config);
extern void gswitchit_kbd_config_layouts_add (GSwitchItKbdConfig *
					      kbd_config,
					      const gchar * layout_name,
					      const gchar * variant_name);

extern void gswitchit_kbd_config_layouts_reset (GSwitchItKbdConfig *
						kbd_config);
extern void gswitchit_kbd_config_options_reset (GSwitchItKbdConfig *
						kbd_config);

extern void gswitchit_kbd_config_options_add (GSwitchItKbdConfig *
					      kbd_config,
					      const gchar * group_name,
					      const gchar * option_name);
extern gboolean gswitchit_kbd_config_options_is_set (GSwitchItKbdConfig *
						     kbd_config,
						     const gchar *
						     group_name,
						     const gchar *
						     option_name);

extern gboolean gswitchit_kbd_config_dump_settings (GSwitchItKbdConfig *
						    kbd_config,
						    const char *file_name);

extern void gswitchit_kbd_config_start_listen (GSwitchItKbdConfig *
					       kbd_config,
					       GConfClientNotifyFunc func,
					       gpointer user_data);

extern void gswitchit_kbd_config_stop_listen (GSwitchItKbdConfig *
					      kbd_config);

/**
 * GSwitchItAppletConfig functions - 
 * some of them require GSwitchItKbdConfig as well - 
 * for loading approptiate images
 */
extern void gswitchit_applet_config_init (GSwitchItAppletConfig *
					  applet_config,
					  GConfClient * conf_client,
					  XklEngine * engine);
extern void gswitchit_applet_config_term (GSwitchItAppletConfig *
					  applet_config);

extern void gswitchit_applet_config_load_from_gconf (GSwitchItAppletConfig
						     * applet_config);
extern void gswitchit_applet_config_save_to_gconf (GSwitchItAppletConfig *
						   applet_config);

extern gchar
    * gswitchit_applet_config_get_images_file (GSwitchItAppletConfig *
					       applet_config,
					       GSwitchItKbdConfig *
					       kbd_config, int group);

extern void gswitchit_applet_config_load_images (GSwitchItAppletConfig *
						 applet_config,
						 GSwitchItKbdConfig *
						 kbd_config);
extern void gswitchit_applet_config_free_images (GSwitchItAppletConfig *
						 applet_config);

/* Should be updated on Applet/GConf and Kbd/GConf configuration change */
extern void gswitchit_applet_config_update_images (GSwitchItAppletConfig *
						   applet_config,
						   GSwitchItKbdConfig *
						   kbd_config);

/* Should be updated on Applet/GConf configuration change */
extern void gswitchit_applet_config_activate (GSwitchItAppletConfig *
					      applet_config);

extern void gswitchit_applet_config_start_listen (GSwitchItAppletConfig *
						  applet_config,
						  GConfClientNotifyFunc
						  func,
						  gpointer user_data);

extern void gswitchit_applet_config_stop_listen (GSwitchItAppletConfig *
						 applet_config);

#endif
