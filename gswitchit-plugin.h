/*
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifndef __GSWITCHIT_PLUGIN_H__
#define __GSWITCHIT_PLUGIN_H__

#include <gtk/gtk.h>
#include <gconf/gconf-client.h>
#include <libgswitchit/gswitchit-config.h>

#define MAX_LOCAL_NAME_BUF_LENGTH 512

struct _GSwitchItPlugin;

typedef struct _GSwitchItPluginContainer {
	GConfClient *conf_client;
} GSwitchItPluginContainer;

typedef const struct _GSwitchItPlugin
*(*GSwitchItPluginGetPluginFunc) (void);

typedef gboolean (*GSwitchItPluginInitFunc) (GSwitchItPluginContainer *
					     pc);

typedef void (*GSwitchItPluginGroupChangedFunc) (GtkWidget * notebook,
						 int new_group);

typedef void (*GSwitchItPluginConfigChangedFunc) (const GSwitchItKbdConfig
						  * from,
						  const GSwitchItKbdConfig
						  * to);

typedef int (*GSwitchItPluginWindowCreatedFunc) (const Window win,
						 const Window parent);

typedef void (*GSwitchItPluginTermFunc) (void);

typedef GtkWidget *(*GSwitchItPluginCreateWidget) (void);

typedef GtkWidget *(*GSwitchItPluginDecorateWidget) (GtkWidget * widget,
						     const gint group,
						     const char
						     *group_description,
						     GSwitchItKbdConfig *
						     config);

typedef
void (*GSwitchItPluginConfigureProperties) (GSwitchItPluginContainer *
					    pc, GtkWindow * parent);

typedef struct _GSwitchItPlugin {
	const char *name;

	const char *description;

// implemented
	GSwitchItPluginInitFunc init_callback;

// implemented
	GSwitchItPluginTermFunc term_callback;

// implemented
	GSwitchItPluginConfigureProperties configure_properties_callback;

// implemented
	GSwitchItPluginGroupChangedFunc group_changed_callback;

// implemented
	GSwitchItPluginWindowCreatedFunc window_created_callback;

// implemented
	GSwitchItPluginDecorateWidget decorate_widget_callback;

// non implemented
	GSwitchItPluginConfigChangedFunc config_changed_callback;

// non implemented
	GSwitchItPluginCreateWidget create_widget_callback;

} GSwitchItPlugin;

/**
 * Functions accessible for plugins
 */

extern void gswitchit_plugin_container_init (GSwitchItPluginContainer * pc,
					  GConfClient * conf_client);

extern void gswitchit_plugin_container_term (GSwitchItPluginContainer * pc);

extern void gswitchit_plugin_container_reinit_ui (GSwitchItPluginContainer *
					      pc);

extern guint gswitchit_plugin_get_num_groups (GSwitchItPluginContainer * pc);

extern gchar
    **gswitchit_plugin_load_localized_group_names (GSwitchItPluginContainer *
					      pc);

#endif
