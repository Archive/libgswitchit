/*
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifndef __GSWITCHIT_CONFIG_H__
#define __GSWITCHIT_CONFIG_H__

#include <X11/Xlib.h>

#include <glib.h>
#include <glib/gslist.h>
#include <gdk/gdk.h>
#include <gtk/gtk.h>

#include <gdk-pixbuf/gdk-pixbuf.h>
#include <gconf/gconf-client.h>

#include <libxklavier/xklavier.h>

extern const gchar GSWITCHIT_CONFIG_DIR[];
extern const gchar GSWITCHIT_CONFIG_KEY_DEFAULT_GROUP[];
extern const gchar GSWITCHIT_CONFIG_KEY_GROUP_PER_WINDOW[];
extern const gchar GSWITCHIT_CONFIG_KEY_HANDLE_INDICATORS[];
extern const gchar GSWITCHIT_CONFIG_KEY_LAYOUT_NAMES_AS_GROUP_NAMES[];

extern const gchar GSWITCHIT_KBD_CONFIG_DIR[];
extern const gchar GSWITCHIT_KBD_CONFIG_KEY_MODEL[];
extern const gchar GSWITCHIT_KBD_CONFIG_KEY_LAYOUTS[];
extern const gchar GSWITCHIT_KBD_CONFIG_KEY_OPTIONS[];

/*
 * General configuration
 */
typedef struct _GSwitchItConfig {
	gint default_group;
	gboolean group_per_app;
	gboolean handle_indicators;
	gboolean layout_names_as_group_names;

	/* private, transient */
	GConfClient *conf_client;
	int config_listener_id;
	XklEngine *engine;
} GSwitchItConfig;

/*
 * Keyboard Configuration
 */
typedef struct _GSwitchItKbdConfig {
	gchar *model;
	GSList *layouts;
	GSList *options;

	/* private, transient */
	GConfClient *conf_client;
	int config_listener_id;
	XklEngine *engine;
} GSwitchItKbdConfig;

/**
 * GSwitchItConfig functions
 */
extern void gswitchit_config_init (GSwitchItConfig * config,
				   GConfClient * conf_client,
				   XklEngine * engine);
extern void gswitchit_config_term (GSwitchItConfig * config);

extern void gswitchit_config_load_from_gconf (GSwitchItConfig * config);

extern void gswitchit_config_save_to_gconf (GSwitchItConfig * config);

extern gboolean gswitchit_config_activate (GSwitchItConfig * config);

/* Affected by XKB and XKB/GConf configuration */
extern gchar
    ** gswitchit_config_load_group_descriptions_utf8 (GSwitchItConfig *
						      config,
						      XklConfigRegistry *
						      config_registry);


/* Using DBUS */
extern gboolean
gswitchit_config_load_remote_group_descriptions_utf8 (GSwitchItConfig *
						      config,
						      gchar ***
						      short_group_names,
						      gchar ***
						      full_group_names);

extern void gswitchit_config_lock_next_group (GSwitchItConfig * config);

extern void gswitchit_config_lock_prev_group (GSwitchItConfig * config);

extern void gswitchit_config_restore_group (GSwitchItConfig * config);

extern void gswitchit_config_start_listen (GSwitchItConfig * config,
					   GConfClientNotifyFunc func,
					   gpointer user_data);

extern void gswitchit_config_stop_listen (GSwitchItConfig * config);

/**
 * GSwitchItKbdConfig functions
 */
extern void gswitchit_kbd_config_init (GSwitchItKbdConfig * kbd_config,
				       GConfClient * conf_client,
				       XklEngine * engine);
extern void gswitchit_kbd_config_term (GSwitchItKbdConfig * kbd_config);

extern void gswitchit_kbd_config_load_from_gconf (GSwitchItKbdConfig *
						  kbd_config,
						  GSwitchItKbdConfig *
						  kbd_config_default);

extern void gswitchit_kbd_config_save_to_gconf (GSwitchItKbdConfig *
						kbd_config);

extern void gswitchit_kbd_config_load_from_gconf_backup (GSwitchItKbdConfig
							 * kbd_config);

extern void gswitchit_kbd_config_save_to_gconf_backup (GSwitchItKbdConfig *
						       kbd_config);

extern void gswitchit_kbd_config_load_from_x_initial (GSwitchItKbdConfig *
						      kbd_config);

extern void gswitchit_kbd_config_load_from_x_current (GSwitchItKbdConfig *
						      kbd_config);

extern void gswitchit_kbd_config_start_listen (GSwitchItKbdConfig *
					       kbd_config,
					       GConfClientNotifyFunc func,
					       gpointer user_data);

extern void gswitchit_kbd_config_stop_listen (GSwitchItKbdConfig *
					      kbd_config);

extern gboolean gswitchit_kbd_config_equals (GSwitchItKbdConfig *
					     kbd_config1,
					     GSwitchItKbdConfig *
					     kbd_config2);

extern gboolean gswitchit_kbd_config_activate (GSwitchItKbdConfig *
					       kbd_config);

extern const gchar *gswitchit_kbd_config_merge_items (const gchar * parent,
						      const gchar * child);

extern gboolean gswitchit_kbd_config_split_items (const gchar * merged,
						  gchar ** parent,
						  gchar ** child);

extern gboolean gswitchit_kbd_config_get_descriptions (XklConfigRegistry *
						       config_registry,
						       const gchar * name,
						       gchar **
						       layout_short_descr,
						       gchar **
						       layout_descr,
						       gchar **
						       variant_short_descr,
						       gchar **
						       variant_descr);

extern const gchar *gswitchit_kbd_config_format_full_layout (const gchar
							     *
							     layout_descr,
							     const gchar *
							     variant_descr);

extern gchar *gswitchit_kbd_config_to_string (const GSwitchItKbdConfig *
					      config);

#endif
